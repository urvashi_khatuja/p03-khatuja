//
//  AppDelegate.h
//  doodle
//
//  Created by urvashi khatuja on 2/18/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

