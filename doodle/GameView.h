//
//  GameView.h
//  doodle
//
//  Created by urvashi khatuja on 2/26/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"

@interface GameView : UIView {
    
}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) float tilt;
-(void)arrange:(CADisplayLink *)sender;

@end

