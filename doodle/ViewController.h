//
//  ViewController.h
//  doodle
//
//  Created by urvashi khatuja on 2/18/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameView.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameView *gameView;


@end




