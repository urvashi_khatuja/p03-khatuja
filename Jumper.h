//
//  Jumper.h
//  doodle
//
//  Created by urvashi khatuja on 2/26/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface Jumper : UIView
@property (nonatomic) float dx, dy;  // Velocity
@end

